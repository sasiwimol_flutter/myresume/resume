import 'dart:js';
import 'package:flutter/material.dart';

Column _buildSkillImgColumn(Color color, String label) {
  return Column(
    mainAxisSize: MainAxisSize.min,
    children: [
      Container(
        child: Text(
          label,
          style: TextStyle(
            fontSize: 12,
            fontWeight: FontWeight.w400,
            color: color,
          ),
        ),
      )
    ],
  );
}

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Color color = Theme.of(context).primaryColor;
    Widget proflieSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text(
                    'PROFILE',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                ),
                Text(
                  'Sasiwimol chuchum',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                ),
                Text(
                  'Birthday: 27 May 1999',
                  style: TextStyle(fontSize: 18, height: 1.5),
                ),
                Text(
                  'AGE: 22 year',
                  style: TextStyle(fontSize: 18, height: 1.5),
                ),
                Text(
                  'GENDER: female',
                  style: TextStyle(fontSize: 18, height: 1.5),
                ),
              ],
            ),
          ),
          Icon(
            Icons.favorite,
            color: Colors.pink[500],
          ),
          Text('Love')
        ],
      ),
    );
    Widget contactSection = Container(
      padding: EdgeInsets.only(left: 30, right: 30),
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Text(
                'CONTACT',
                style: TextStyle(
                    fontWeight: FontWeight.bold, fontSize: 20, height: 1.0),
              ),
              Container(
                child: Row(
                  children: [
                    Icon(
                      Icons.room,
                      color: Colors.blueGrey,
                    ),
                    Expanded(
                        child: Container(
                      child: Text(
                        '126 Lak Phrao Rd., Wang Thong Lang, '
                        'Wang Thong Lang, Bangkok 10310',
                        softWrap: true,
                        style: TextStyle(
                          fontSize: 16,
                          height: 1.5,
                        ),
                      ),
                    ))
                  ],
                ),
              ),
              Container(
                child: Row(
                  children: [
                    Icon(
                      Icons.phone,
                      color: Colors.blueGrey,
                    ),
                    Expanded(
                        child: Container(
                      child: Text(
                        '0-84972-xxxx',
                        style: TextStyle(fontSize: 16, height: 1.5),
                        softWrap: true,
                      ),
                    ))
                  ],
                ),
              ),
              Container(
                child: Row(
                  children: [
                    Icon(
                      Icons.email,
                      color: Colors.blueGrey,
                    ),
                    Expanded(
                        child: Container(
                      child: Text(
                        ' 61160269@go.buu.ac.th',
                        style: TextStyle(fontSize: 16, height: 1.5),
                      ),
                    ))
                  ],
                ),
              ),
            ],
          )),
        ],
      ),
    );
    Widget educationSection = Container(
      padding: EdgeInsets.only(top: 32, left: 30, right: 30),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  //padding: const EdgeInsets.only(bottom: 8),
                  child: Text(
                    'EDUCATION',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  ),
                ),
                Text(
                  '2018 - Present',
                  softWrap: true,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                    height: 1.5,
                  ),
                ),
                Text(
                  'B.Sc. (Computer science) – in progress Faculty of  Informatics '
                  'Burapha University 169 Long Had Bang Saen Rd. Chon Buri 20130',
                  softWrap: true,
                  style: TextStyle(
                    fontSize: 16,
                    height: 1.5,
                  ),
                ),
                Text(
                  '2015 - 2018',
                  softWrap: true,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                    height: 1.5,
                  ),
                ),
                Text(
                  'High school certificate Triamudomsuksapattanakarn Ratchada  School '
                  'Ratchadaphisek Rd. Huai Kwang, Bangkok  10310',
                  softWrap: true,
                  style: TextStyle(
                    fontSize: 16,
                    height: 1.5,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
    Widget skillSection = Container(
      padding: EdgeInsets.only(left: 30, right: 30),
      child: Row(
        children: [
          Container(
            child: Text('Skills',
                style: TextStyle(
                    fontWeight: FontWeight.bold, fontSize: 20, height: 1.5)),
          ),
          Expanded(
              child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Column(
                    children: [
                      Image.asset(
                        'images/html1.png',
                        width: 50,
                        height: 50,
                      ),
                      _buildSkillImgColumn(color, 'HTML')
                    ],
                  ),
                  Column(
                    children: [
                      Image.asset(
                        'images/js.png',
                        width: 50,
                        height: 50,
                      ),
                      _buildSkillImgColumn(color, 'Javascript')
                    ],
                  ),
                  Column(
                    children: [
                      Image.asset(
                        'images/css3.png',
                        width: 50,
                        height: 50,
                      ),
                      _buildSkillImgColumn(color, 'CSS-3')
                    ],
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Column(
                    children: [
                      Image.asset(
                        'images/gitlab.png',
                        width: 50,
                        height: 50,
                      ),
                      _buildSkillImgColumn(color, 'GITLAB')
                    ],
                  ),
                  Column(
                    children: [
                      Image.asset(
                        'images/vue-js.png',
                        width: 50,
                        height: 50,
                      ),
                      _buildSkillImgColumn(color, 'VUE.js')
                    ],
                  ),
                ],
              ),
            ],
          )),
        ],
      ),
    );

    return MaterialApp(
        title: 'Myresume',
        home: Scaffold(
          appBar: AppBar(
            title: const Text('My resume'),
          ),
          body: ListView(children: [
            Image.asset(
              'images/beer.jpg',
              width: 500,
              height: 400,
              fit: BoxFit.cover,
            ),
            proflieSection,
            contactSection,
            educationSection,
            skillSection,
          ]),
        ));
  }
}
